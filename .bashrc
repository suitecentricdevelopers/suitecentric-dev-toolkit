#!/usr/bin/env bash
# SuiteCentric Dev Toolkit - Bash Configuration
# Copyright (c) 2021 SuiteCentric LLC

TOOLKIT_DIR="$(dirname "$(readlink -n ~/.bashrc)")"
export PATH=/opt/homebrew/bin:"$PATH"
BREW_PREFIX="$(brew --prefix)"

########## Functions and Aliases ##########
# Define your own shortcuts for common terminal commands here.

source "$TOOLKIT_DIR"/sca.sh

# Reload the current shell completely
alias reload='exec $SHELL -l'
# View the status of Git's working directory
alias gs='git status'

########## Commands and autocompletion ##########

# Expand multiple subdirectories using the globstar (**) option
shopt -s globstar 2> /dev/null
# Ensure that command history is appended to and not overwritten
shopt -s histappend

# Navigate command history matching typed input using up/down arrow keys
bind '"\e[A": history-search-backward' 2> /dev/null
bind '"\e[B": history-search-forward' 2> /dev/null
# Automatically add trailing slash to autocompleted directory symlinks
bind 'set mark-symlinked-directories on' 2> /dev/null
# Make autocompletion case-insensitive
bind 'set completion-ignore-case on' 2> /dev/null
# Keep duplicate entries out of command history
export HISTCONTROL='ignoreboth:erasedups'

# If shell is Bash 4 or newer
if [ ${BASH_VERSINFO[0]} -ge 4 ]; then

	# If Bash Completion is installed
	if [ -f /usr/local/share/bash-completion/bash_completion ]; then
		# Load all completions
		export BASH_COMPLETION_COMPAT_DIR="/usr/local/etc/bash_completion.d"
		source /usr/local/share/bash-completion/bash_completion
	fi

fi

########## Shell color mappings ##########
# Don't change these; if you want to change the actual colors, navigate to
# Terminal > Preferences > Profiles > ANSI Colors


# Standard colors
export BLACK='0;30'
export RED='0;31'
export GREEN='0;32'
export YELLOW='0;33'
export BLUE='0;34'
export MAGENTA='0;35'
export CYAN='0;36'
export WHITE='0;37'

# Bold colors
export BLACK_BOLD='1;30'
export RED_BOLD='1;31'
export GREEN_BOLD='1;32'
export YELLOW_BOLD='1;33'
export BLUE_BOLD='1;34'
export MAGENTA_BOLD='1;35'
export CYAN_BOLD='1;36'
export WHITE_BOLD='1;37'

# Use green, underlined text for grep matches
export GREP_COLOR='4;32'

# Use fancy diffing and case-insensitivity for git diffs and pagers
export GIT_PAGER='diff-so-fancy | less --tabs=4 -R --no-init --quit-if-one-screen --IGNORE-CASE'


########## Node and nvm ##########
# The following section loads Node and nvm, the #Node version switcher; a
# compatibility function has also been added to ease the #transition from n to
# nvm

# Load nvm
source "$TOOLKIT_DIR"/load_nvm.sh
# Ease transition from n to nvm
n() {
	if [ -n "$1" ]; then
		nvm use "$1" 2> /dev/null || nvm install "$1"
	else
		nvm list
	fi
}

########## Command prompt string ##########
# The following section defines the text that shows before each command you
# type; feel free to change the color mappings to suit your personal tastes

# Detect the node version for this project and switch to it
__detect_node_version() {
	local nvmrc_contents="$(cat .nvmrc 2> /dev/null)"
	if [ -z "$nvmrc_contents" ]; then
		nvmrc_contents="$(cat ../.nvmrc 2> /dev/null)"
	fi
	# If an .nvmrc exists in the current directory (that we just entered),
	# switch to that node version if it's not already
	if [[ -n "$nvmrc_contents" && "$(node -v | cut -c2-)" != "$nvmrc_contents" && "$CURRENT_NODE_AUTO_SWITCH_PWD" != "$PWD" ]]; then
		export CURRENT_NODE_AUTO_SWITCH_PWD="$PWD"
		nvm use --silent &> /dev/null || nvm install "$(cat .nvmrc)"
	fi
}

# Outputs ANSI escape sequence for the given color code
__set_color() {
	echo -n "\[\e[${1}m\]"
}

# Reset color escape sequences
__reset_color() {
	__set_color 0
}

# Output a succinct and useful interactive prompt
# Escape sequences: <https://ss64.com/bash/syntax-prompt.html>
__output_ps1() {

	# Define a local constant for the separator between items in the prompt
	local SEPARATOR=' '

	# Output name of current working directory (with ~ denoting HOME)
	__set_color $CYAN
	echo -n '\W'
	echo -n "$SEPARATOR"

	# If working directory is a Node-based project
	if [ -f package.json ] && type node &> /dev/null; then

		# Output version of global Node
		__set_color $BLUE
		echo -n "node$(node --version | grep -Eo 'v[0-9]+\.[0-9]+' | cut -c 2-)"
		echo -n "$SEPARATOR"

	fi

	# If working directory is (or resides in) a git repository
	if git rev-parse --git-dir &> /dev/null; then

		# Output name of current branch
		__set_color $BLACK
		echo -n "$(git rev-parse --abbrev-ref HEAD 2> /dev/null)"
		echo -n "$SEPARATOR"

	fi

	# Output $ for user and # for root
	__set_color $GREEN
	echo -ne '$ '
	__reset_color

}

# Run the following before each new command
__update_prompt_command() {

	if [ -z "$DISABLE_NODE_AUTO_SWITCH" ]; then
		__detect_node_version
	fi
	PS1="$(__output_ps1)"
	# Append in-memory command history to file
	history -a
	# Ensure current working directory carries to new tabs
	update_terminal_cwd 2> /dev/null

}
PROMPT_COMMAND="__update_prompt_command"


########## Other miscellaneous functions ##########

# A global command for interacting with the toolkit repository
suitecentric() {
	local subcommand="$1"
	if [ "$subcommand" == 'install' ]; then
		source "$TOOLKIT_DIR"/install.sh
	elif [ "$subcommand" == 'pull' ]; then
		pushd "$TOOLKIT_DIR" > /dev/null || return
		git pull --ff-only
		popd > /dev/null || return
		reload
	else
		>&2 echo "usage: ${FUNCNAME[0]} <command>"
		>&2 echo "Available commands: pull, install"
		return 1
	fi
}
_suitecentric() {
	cur=${COMP_WORDS[COMP_CWORD]}
	prev=${COMP_WORDS[COMP_CWORD-1]}
	if [ "$prev" == 'suitecentric' ]; then
		COMPREPLY=( $(compgen -W 'install pull' -- $cur) )
	fi
}
complete -F _suitecentric suitecentric

# Source private .bashrc
source "$TOOLKIT_DIR"/private/.bashrc 2> /dev/null
