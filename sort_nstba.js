// SuiteCentric Dev Toolkit - Script to sort ~/.nstba file
// Copyright (c) 2021 SuiteCentric LLC

var fs = require('fs');
var os = require('os');
var path = require('path');

var NSTBA_PATH = path.join(os.homedir(), '.nstba');
var tbaJson = JSON.parse(String(fs.readFileSync(NSTBA_PATH)));

// Sort the display names in the .nstba file by creating a new object with the
// keys (i.e. display names) added in sorted order
var sortedTbaJson = {};
Object.keys(tbaJson).sort(function (key1, key2) {
	// Sort keys in a case-insensitive manner
	key1 = key1.toLowerCase();
	key2 = key2.toLowerCase();
	if (key1 < key2) {
		return -1;
	} else if (key1 > key2) {
		return 1;
	} else {
		return 0;
	}
}).forEach(function (key) {
	sortedTbaJson[key] = tbaJson[key];
});

fs.writeFileSync(NSTBA_PATH, JSON.stringify(sortedTbaJson, null, 2));