#!/usr/bin/env bash
# SuiteCentric Dev Toolkit - Installer Script
# Copyright (c) 2021 SuiteCentric LLC

TOOLKIT_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"

echo "Installing and configuring system for SuiteCentric development..."

source "$TOOLKIT_DIR"/helper_install_functions.sh

########## macOS Preferences ##########

if type defaults &> /dev/null; then
	# Show hidden files in Finder
	defaults write com.apple.finder AppleShowAllFiles -bool true
	killall Finder
fi

########## Homebrew ###########

if ! type brew &> /dev/null && type xcode-select &> /dev/null; then
	# Homebrew requires Apple's command line developer tools
	xcode-select --install
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

export PATH=/opt/homebrew/bin:"$PATH"
BREW_PREFIX="$(brew --prefix)"
preload_brew_pkg_list
preload_cask_list

########## Autocompletion ###########

install_brew_pkg bash
if [ -f /usr/local/bin/bash ] && [ "$SHELL" != /usr/local/bin/bash ]; then
	sudo chsh -s "$BREW_PREFIX"/bin/bash "$USER"
fi
install_brew_pkg bash-completion@2

########## Git ###########

install_brew_pkg git
# Configure better git diffing with diff-so-fancy
install_brew_pkg diff-so-fancy
git config --global credential.helper 'osxkeychain'
git config --global color.diff-highlight.oldNormal 'red bold'
git config --global color.diff-highlight.oldHighlight 'red bold 52'
git config --global color.diff-highlight.newNormal 'green bold'
git config --global color.diff-highlight.newHighlight 'green bold 22'
# Show diff of staged changes
git config --global alias.diffc 'diff --cached'
# Enable 'color words' diff option
git config --global diff.wordRegex '[A-Z0-9]+|[A-Z0-9]?[a-z0-9]+|[^[:space:]]'
git config --global alias.diffcw 'diff --color-words'
# Call `git pushu` when pushing a new branch for the first time; after this, you
# can always call `git push`
git config --global alias.pushu '!git push -u origin $(git rev-parse --abbrev-ref HEAD)'

########## Node ###########


# Install Node and relevant packages
if ! is_cmd_installed nvm; then
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
	mkdir -p ~/.nvm
	source "$TOOLKIT_DIR"/load_nvm.sh
	nvm install 10.16.3
	nvm install 12.16.3
	nvm alias default 12.16.3
fi

########## Node packages ###########
preload_npm_pkg_list
install_npm_pkg eslint
install_npm_pkg vsce

########## Visual Studio Code ###########
if [ ! -e /Applications/Visual\ Studio\ Code.app ]; then
	install_cask visual-studio-code
	install_vscode_package EditorConfig.EditorConfig
	install_vscode_package dbaeumer.vscode-eslint
	install_vscode_package caleb531.sca-for-vscode
fi

# Install Slack if it doesn't exist
if [ ! -e /Applications/Slack.app ]; then
	install_cask slack
fi

# Install Google Chrome if it doesn't exist
if [ ! -e /Applications/Google\ Chrome.app ]; then
	install_cask google-chrome
fi

########## Finishing up ###########

# Move ESLint config into place
mkdir -p ~/SuiteCentric
mkdir -p ~/suitecentric
mkdir -p ~/Documents
ln -sf "$TOOLKIT_DIR"/.eslintrc.js ~/suitecentric/.eslintrc.js
ln -sf "$TOOLKIT_DIR"/.eslintrc.js ~/SuiteCentric/.eslintrc.js
ln -sf "$TOOLKIT_DIR"/.eslintrc.js ~/Documents/.eslintrc.js

# Set and load user bash configuration, but only if a .bashrc symlink does not
# already exist in the user's home directory
if [ ! -L ~/.bashrc ]; then
	cp -n ~/.bashrc ~/.bashrc.bak 2> /dev/null
	cp -n ~/.bash_profile ~/.bash_profile.bak 2> /dev/null
	ln -sf "$TOOLKIT_DIR"/.bashrc ~/.bashrc
	ln -sf "$TOOLKIT_DIR"/.bashrc ~/.bash_profile
fi

# Allow for per-machine private configuration
mkdir -p "$TOOLKIT_DIR"/private
touch "$TOOLKIT_DIR"/private/.bashrc

echo "Installation complete. You're all set!"
echo "Please close your terminal window and open a fresh one"
