#!/usr/bin/env bash
# SuiteCentric Dev Toolkit - Helper Functions for install.sh
# Copyright (c) 2021 SuiteCentric LLC

preload_brew_pkg_list() {
	BREW_PKG_LIST="$(brew list --formula -1)"
}

preload_npm_pkg_list() {
	NPM_PKG_LIST="$(npm list --global --depth=0)"
}

preload_cask_list() {
	CASK_LIST="$(brew list --cask -1)"
}

preload_apm_pkg_list() {
	APM_PKG_LIST="$(apm list --installed --bare)"
}

preload_vscode_pkg_list() {
	VSCODE_PKG_LIST="$(code --list-extensions)"
}

is_cmd_installed() {
	type "$1" &> /dev/null
}

is_brew_pkg_installed() {
	echo "$BREW_PKG_LIST" | grep --extended-regexp --quiet "^$1(@|\$)"
}

is_npm_pkg_installed() {
	echo "$NPM_PKG_LIST" | grep --quiet " $1@"
}

is_cask_installed() {
	echo "$CASK_LIST" | grep --quiet "^$1\$"
}

is_apm_pkg_installed() {
	echo "$APM_PKG_LIST" | grep --quiet "^$1@"
}

is_vscode_pkg_installed() {
	echo "$VSCODE_PKG_LIST" | grep --quiet "^$1$"
}

install_brew_pkg() {
	if ! is_brew_pkg_installed "$1"; then
		brew install "$@"
	fi
}

pin_brew_pkg() {
	brew pin "$@" 2> /dev/null
}

install_npm_pkg() {
	if ! is_npm_pkg_installed "$1"; then
		echo "Installing $1..."
		npm install --global "$@"
	fi
}

install_cask() {
	if ! is_cask_installed "$1"; then
		echo "Installing $1..."
		brew install --cask "$@"
	fi
}

install_apm_pkg() {
	if ! is_apm_pkg_installed "$1"; then
		apm install "$@"
	fi
}

install_vscode_pkg() {
	if ! is_vscode_pkg_installed "$1"; then
		code --install-extension "$@"
	fi
}
