# SuiteCentric Developer Toolkit

*Copyright (c) 2021 SuiteCentric LLC*  
*Released under the MIT License*

This repository contains scripts and configuration for SuiteCentric
development. **This toolkit is mostly written for macOS (with some Linux
compatibility).** You will most likely have mixed results installing on Windows.

## Steps

### 1. Clone repository to home directory

The toolkit repository can be placed in any directory of your choosing. I might
recommend directly in your home folder, or a folder alongside your other SCA
repositories.

```sh
cd ~
git clone git@bitbucket.org:suitecentricdevelopers/suitecentric-dev-toolkit.git
```

### 2. Run install script

Once you've cloned the repository, cd to the project directory and run
`install.sh`. This will install all the necessary software dependencies and
configuration for SuiteCentric development, if necessary.

```sh
source install.sh
```

### 3. Install Terminal theme

To use a prettier Terminal theme, open the `Monokai.terminal` theme, then
navigate to **Terminal > Preferences > Profiles** to set it as the default
theme. Feel free to make color adjustments within that same preferences pane.

## Updates

To pull the latest updates to this toolkit, you can run the following command
from any directory:

```sh
suitecentric pull
```

## Features

### Command Line

#### gf

The `gf` command fetches the current extensions or theme. It always prompts you for a token to determine the account to fetch from.

#### gl

The `gl` command is exactly equivalent to `gulp local`. It works for themes and
extensions, not just base SCA codebases.

#### gc

The `gc` command is exactly equivalent to `gulp clean`.

#### gcl

The `gcl` command is equivalent to running `gulp clean` followed by `gulp
local`. It also works for themes and extensions. This is the recommended command
to use for local development.

#### gdp

The `gdp` command deploys the current SCA codebase, theme, or extension. It
always prompts you your email, password, and SSP application to deploy to.

```sh
gdp
```

#### gdpr

The `gdpr` command (the `r` stands for *remember*) is like `gdp`, except it
remembers the email address and SSP application folder used in your last deploy
(so you only need to type your password). It will kindly warn you when deploying
to a production account.

#### gdpk and gdprk

The `gdpk` and `gdprk` commands are like the above `gdp` and `gdpr` commands,
with the exception that `gdpk` and `gdprk` skip compilation and go directly to
the email/password prompt. This is useful if you are deploying the same codebase
to multiple SSP applications, or if you cancel the email/password prompt from a
previous run.

#### int

The `int` command (which stands for *integrate*) deploys your SCA code to a
sandbox/staging environment per our SuiteCentric integration branch workflow. It
takes care of switching to the branch named `integration` (or whichever branch
you specify), pulling/pushing commits, and deploying your work to NetSuite.

If you have conflicts at any point, the command aborts so you can resolve them.
If this is the case, you can continue the deploy by running `int` again once
you've resolved conflicts.

For example, to deploy your changes from the `integration` branch:

```sh
int
```

You can also pass an alternative branch name to use as the integration branch.
This syntax supports tab completion so you don't have to type the full branch
name:

```sh
int friday-demo
```

#### intr

The `intr` command is like `gdp`, except it remembers the email address and SSP
application folder used in your last deploy (so you only need to type your
password). It will kindly warn you when deploying to a production account.

```sh
intr
```

```sh
intr friday-demo
```

#### intr

The `intn` command performs the same Git operations as `int`, except it does not
actually initiate an SCA deploy. In other words, it only merges your current
branch into the `integration` branch (or the specified branch), but does not
proceed to deploy from that point.

This command is useful if you need to merge multiple branches into an
integration-type branch without needing to repeatedly cancel deploys.

```sh
intn
```

```sh
intn friday-demo
```

#### pr

The `pr` command opens the *New Pull Request* page in your web browser. The
command will automatically detect whether your repository is hosted on Bitbucket
or GitHub, and will accordingly open the correct page.
