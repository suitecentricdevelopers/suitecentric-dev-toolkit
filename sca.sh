#!/usr/bin/env bash
# SuiteCentric Dev Toolkit - SCA-specific Helpers
# Copyright (c) 2021 SuiteCentric LLC

TOOLKIT_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)"

# SCA functions/aliases

# The way nvm segments node versions means that global npm packages need to be
# installed for every single node version you need to switch between; to work
# around this, we simply alias gulp to the project-local node_modules binary
gulp() {
	if [ -f ./node_modules/.bin/gulp ]; then
		./node_modules/.bin/gulp "$@"
	else
		>&2 echo 'gulp: command not found'
	fi
}

nsdeploy() {
	cat .nsdeploy SC_*_Live/.nsdeploy gulp/config/.nsdeploy 2> /dev/null
	echo ''
}
configjson() {
	cat .nsdeploy gulp/config/config.json 2> /dev/null
	echo ''
}
isextension() {
	configjson | grep -q '"extensionMode":\s*true'
}
gc() {
	__cd_sca_subdir
	rm -rf LocalDistribution* bin/LocalDistribution*
	rm -rf Workspace/Extras/.DS_Store
}
gl() {
	if [ -d Workspace ]; then
		__gl_fix
		if isextension; then
			gulp extension:local "$@"
		else
			gulp theme:local "$@"
		fi
	else
		__cd_sca_subdir
		gulp local "$@"
	fi
}
# Fix gulp theme:local for themes
__gl_fix() {
	mkdir -p Workspace/Extras/Extensions
	mkdir -p Workspace/Extras/Overrides
	local theme_dir=Workspace/"$(ls -1 Workspace | grep -v Extras | head -n 1)"
	if [ -n "$theme_dir" ]; then
		mkdir -p "$theme_dir"/Overrides
	fi
}
# Fix "out.writeUint32LE is not a function" error after npm install of an
# Elbrus or Kilimanjaro project
__uint32_fix() {
	cd node_modules/gulp-ttf2woff || return
	npm install ttf2woff@2.0.2
	cd ../gulp-ttf2eot || return
	npm install ttf2eot@2.0.0
	cd ../.. || return
}
# Sort the nstba file by display name
__sort_nstba() {
	node "$TOOLKIT_DIR"/sort_nstba.js 2> /dev/null
}
# Fix permissions for local Token-Based Authentication (TBA) storage
__nstba_fix() {
	if [ -f ~/.nstba ]; then
		chmod u+r ~/.nstba
	fi
}
__cd_sca_subdir() {
	cd SC_*_Live 2> /dev/null || return
}
gcl() {
	gc
	gl "$@"
}
gf() {
	if isextension; then
		gulp extension:fetch --to
	else
		gulp theme:fetch --to
	fi
}
alias gtf='gulp theme:fetch --to'
alias gtfr='gulp theme:fetch'
alias gef='gulp extension:fetch --to'
alias gefr='gulp extension:fetch'
alias gec='gulp extension:create'
alias gum='gulp update-manifest'

alias gdsr='gulp deploy --m sandbox --no-backup --nobump'
alias gds='gdsr --to'
alias gdsk='gds --skip-compilation'
alias gdsrk='gdsr --skip-compilation'

__gdp() {
	__sort_nstba
	if [ -d Workspace ]; then
		__gl_fix
		__nstba_fix
		if isextension; then
			gulp extension:deploy "$@"
		else
			gulp theme:deploy "$@"
		fi
	else
		__cd_sca_subdir
		gulp deploy --no-backup --nobump "$@"
	fi
}
gdpr() {
	if [ "$1" != '--skip-prod-confirmation' ]; then
		__confirm_deploy_if_prod
	fi
	__gdp "$@"
}
alias gdprk='gdpr --skip-compilation'
alias gdp='__gdp --to'
alias gdpk='__gdp --to --skip-compilation'

gdz() {
	local deploy_dist_dir="$(find . -type d -name DeployDistribution -print -quit)"
	if [ -n "$deploy_dist_dir" ]; then
		local zip_dest=${1:-~/Desktop/DeployDistribution.zip}
		pushd "$deploy_dist_dir" > /dev/null || return
		rm -rf "$zip_dest"
		zip -r "$zip_dest" ./*
		popd > /dev/null || return
	else
		>&2 echo "gdz: DeployDistribution: no such file or directory"
	fi
}

__confirm_deploy_if_prod() {
	if nsdeploy | grep -E --quiet '"account":\s*"[0-9]+"'; then
		read -rp $'\e[1;33mDEPLOYING TO PRODUCTION\npress Enter to proceed or control-C to abort. \e[0m'
	fi
}

__int() {
	local deploy_cmd="$1"
	local orig_branch="$(git rev-parse --abbrev-ref HEAD)"
	local integration_branch="${2:-integration}"

	# Prompt user if deploying to production
	if [ "$deploy_cmd" == 'gdpr' ]; then
		__confirm_deploy_if_prod
	fi

	git checkout "$integration_branch"
	if [ $? == 0 ]; then
		git pull origin "$integration_branch"
		git merge --no-ff --no-edit "$orig_branch"
		if [ $? == 0 ]; then
			git push
			if [ "$deploy_cmd" == gdp ]; then
				gdp
			elif [ "$deploy_cmd" == gdpr ]; then
				gdpr --skip-prod-confirmation
			fi
			if [ $? == 0 ]; then
				git stash
			fi
			if [ $? == 0 ]; then
				git checkout "$orig_branch"
			fi
		fi
	fi
}
int() {
	__int gdp "$@"
}
intr() {
	__int gdpr "$@"
}
intn() {
	__int -- "$@"
}
_int() {
	cur=${COMP_WORDS[COMP_CWORD]}
	prev=${COMP_WORDS[COMP_CWORD-1]}
	if [[ "$prev" == int* ]]; then
		local branches="$(git branch | grep -Eo '[A-Za-z0-9\-]+' | xargs)"
		COMPREPLY=( $(compgen -W "$branches" -- "$cur") )
	fi
}
complete -o default -F _int int
complete -o default -F _int intr
complete -o default -F _int intn

# Create a pull request on Bitbucket or GitHub
pr() {
	git push
	if [ $? != 0 ]; then
		return
	fi
	local branch_name="$(git rev-parse --abbrev-ref HEAD)"
	local repo_url="$(git config --get remote.origin.url | sed 's/\.git//')"
	if echo "$repo_url" | grep -q 'bitbucket'; then
		# Bitbucket
		local repo_url="$(echo "$repo_url" \
			| sed 's git@bitbucket.org: https://bitbucket.org/ g')"
		local pr_url="${repo_url}/pull-requests/new?source=${branch_name}&t=1"
	elif echo "$repo_url" | grep -q 'github'; then
		# GitHub
		local repo_url="$(echo "$repo_url" \
			| sed 's git@github.com: https://github.com/ g')"
		local pr_url="${repo_url}/compare/master...${branch_name}"
	fi
	if [ -n "$pr_url" ]; then
		echo "$pr_url"
		open "$pr_url"
	else
		>&2 echo "PR URL is empty"
	fi
}
