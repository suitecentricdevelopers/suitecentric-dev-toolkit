// SuiteCentric Dev Toolkit - ESLint Configuration
// Copyright (c) 2021 SuiteCentric LLC
// Configured for ESLint 3.15.0

module.exports = {

  /**
   * Allowed JavaScript environments
   */

  env: {

    // browser global variables
    'browser': true,
    // enable all ECMAScript 6 features except for modules (this automatically
    // sets the ecmaVersion parser option to 6)
    'es6': true,
    // Node.js global variables and Node.js scoping
    'node': true

  },

  globals: {
    'CMS': true,
    'define': true,
    'getExtensionAssetsPath': true,
    'nlapiAddDays': true,
    'nlapiAddMonths': true,
    'nlapiAttachRecord': true,
    'nlapiCancelLineItem': true,
    'nlapiCommitLineItem': true,
    'nlapiCopyRecord': true,
    'nlapiCreateAssistant': true,
    'nlapiCreateCSVImport': true,
    'nlapiCreateCurrentLineItemSubrecord': true,
    'nlapiCreateSearch': true,
    'nlapiCreateSubrecord': true,
    'nlapiCreateEmailMerger': true,
    'nlapiCreateError': true,
    'nlapiCreateFile': true,
    'nlapiCreateForm': true,
    'nlapiCreateList': true,
    'nlapiCreateRecord': true,
    'nlapiCreateReportDefinition': true,
    'nlapiCreateReportForm': true,
    'nlapiCreateTemplateRenderer': true,
    'nlapiDateToString': true,
    'nlapiDeleteFile': true,
    'nlapiDeleteRecord': true,
    'nlapiDetachRecord': true,
    'nlapiDisableField': true,
    'nlapiDisableLineItemField': true,
    'nlapiEditCurrentLineItemSubrecord': true,
    'nlapiEditSubrecord': true,
    'nlapiEncrypt': true,
    'nlapiEscapeXML': true,
    'nlapiExchangeRate': true,
    'nlapiFindLineItemMatrixValue': true,
    'nlapiFindLineItemValue': true,
    'nlapiFormatCurrency': true,
    'nlapiGetContext': true,
    'nlapiGetCurrentLineItemDateTimeValue': true,
    'nlapiGetCurrentLineItemIndex': true,
    'nlapiGetCurrentLineItemMatrixValue': true,
    'nlapiGetCurrentLineItemText': true,
    'nlapiGetCurrentLineItemValue': true,
    'nlapiGetCurrentLineItemValues': true,
    'nlapiGetDateTimeValue': true,
    'nlapiGetDepartment': true,
    'nlapiGetJobManager': true,
    'nlapiGetField': true,
    'nlapiGetFieldText': true,
    'nlapiGetFieldTexts': true,
    'nlapiGetFieldValue': true,
    'nlapiGetFieldValues': true,
    'nlapiGetLineItemCount': true,
    'nlapiGetLineItemDateTimeValue': true,
    'nlapiGetLineItemField': true,
    'nlapiGetLineItemMatrixField': true,
    'nlapiGetLineItemMatrixValue': true,
    'nlapiGetLineItemText': true,
    'nlapiGetLineItemValue': true,
    'nlapiGetLineItemValues': true,
    'nlapiGetLocation': true,
    'nlapiGetLogin': true,
    'nlapiGetMatrixCount': true,
    'nlapiGetMatrixField': true,
    'nlapiGetMatrixValue': true,
    'nlapiGetNewRecord': true,
    'nlapiGetOldRecord': true,
    'nlapiGetRecordId': true,
    'nlapiGetRecordType': true,
    'nlapiGetRole': true,
    'nlapiGetSubsidiary': true,
    'nlapiGetUser': true,
    'nlapiInitiateWorkflow': true,
    'nlapiInsertLineItem': true,
    'nlapiInsertLineItemOption': true,
    'nlapiInsertSelectOption': true,
    'nlapiIsLineItemChanged': true,
    'nlapiLoadFile': true,
    'nlapiLoadRecord': true,
    'nlapiLoadSearch': true,
    'nlapiLogExecution': true,
    'nlapiLookupField': true,
    'nlapiMergeRecord': true,
    'nlapiMergeTemplate': true,
    'nlapiOutboundSSO': true,
    'nlapiPrintRecord': true,
    'nlapiRefreshLineItems': true,
    'nlapiRefreshPortlet': true,
    'nlapiRemoveCurrentLineItemSubrecord': true,
    'nlapiRemoveLineItem': true,
    'nlapiRemoveLineItemOption': true,
    'nlapiRemoveSelectOption': true,
    'nlapiRemoveSubrecord': true,
    'nlapiRequestURL': true,
    'nlapiRequestURLWithCredentials': true,
    'nlapiResizePortlet': true,
    'nlapiResolveURL': true,
    'nlapiScheduleScript': true,
    'nlapiSearchDuplicate': true,
    'nlapiSearchGlobal': true,
    'nlapiSearchRecord': true,
    'nlapiSelectLineItem': true,
    'nlapiSelectNewLineItem': true,
    'nlapiSelectNode': true,
    'nlapiSelectNodes': true,
    'nlapiSelectValue': true,
    'nlapiSelectValues': true,
    'nlapiSendCampaignEmail': true,
    'nlapiSendEmail': true,
    'nlapiSendFax': true,
    'nlapiSetCurrentLineItemDateTimeValue': true,
    'nlapiSetCurrentLineItemMatrixValue': true,
    'nlapiSetCurrentLineItemText': true,
    'nlapiSetCurrentLineItemValue': true,
    'nlapiSetCurrentLineItemValues': true,
    'nlapiSetDateTimeValue': true,
    'nlapiSetFieldText': true,
    'nlapiSetFieldTexts': true,
    'nlapiSetFieldValue': true,
    'nlapiSetFieldValues': true,
    'nlapiSetLineItemDateTimeValue': true,
    'nlapiSetLineItemValue': true,
    'nlapiSetMatrixValue': true,
    'nlapiSetRecoveryPoint': true,
    'nlapiSetRedirectURL': true,
    'nlapiStringToDate': true,
    'nlapiStringToXML': true,
    'nlapiSubmitCSVImport': true,
    'nlapiSubmitField': true,
    'nlapiSubmitFile': true,
    'nlapiSubmitRecord': true,
    'nlapiTransformRecord': true,
    'nlapiTriggerWorkflow': true,
    'nlapiViewCurrentLineItemSubrecord': true,
    'nlapiViewLineItemSubrecord': true,
    'nlapiViewSubrecord': true,
    'nlapiXMLToPDF': true,
    'nlapiXMLToString': true,
    'nlapiYieldScript': true,
    'nlobjError': true,
    'nlobjSearchColumn': true,
    'nlobjSearchFilter': true,
    'nsglobal': true,
    'SC': true,
    'session': true,
    'suitecentric': true
  },

  /**
   * Enabled ESLint rules
   */

  rules: {

    /**
     * Possible Errors
     */

    // disallow assignment operators in conditional expressions
    'no-cond-assign': ['error', 'always'],
    // disallow the use of console
    // 'no-console': ['warn', {'allow': ['error']}],
    // disallow the use of debugger
    'no-debugger': ['warn'],
    // disallow duplicate arguments in function definitions
    'no-dupe-args': ['error'],
    // disallow duplicate keys in object literals
    'no-dupe-keys': ['error'],
    // disallow invalid regular expression strings in RegExp constructors
    'no-invalid-regexp': ['error'],
    // disallow labeled statements
    'no-labels': ['error'],
    // disallow calling global object properties as functions
    'no-obj-calls': ['error'],
    // disallow multiple spaces in regular expressions
    'no-regex-spaces': ['error'],
    // disallow sparse arrays
    'no-sparse-arrays': ['error'],
    // disallow confusing multiline expressions
    'no-unexpected-multiline': ['error'],
    // disallow unreachable code after return, throw, continue, and break
    // statements
    'no-unreachable': ['error'],
    // disallow control flow statements in finally blocks
    'no-unsafe-finally': ['error'],
    // require calls to isNaN() when checking for NaN
    'use-isnan': ['error'],
    // enforce comparing typeof expressions against valid strings
    'valid-typeof': ['error'],

    /**
     * Best Practices
     */

    // enforce return statements in callbacks of array methods
    'array-callback-return': ['error'],
    // enforce consistent newlines before and after dots
    'dot-location': ['error', 'property'],
    // require the use of === and !==
    'eqeqeq': ['error', 'always'],
    // disallow the use of arguments.caller or arguments.callee
    'no-caller': ['error'],
    // disallow division operators explicitly at the beginning of regular
    // expressions
    'no-div-regex': ['error'],
    // disallow the use of eval
    'no-eval': ['error'],
    // disallow extending native types
    'no-extend-native': ['error'],
    // disallow assignments to native objects or read-only global variables
    'no-global-assign': ['error'],
    // disallow variable and function declarations in the global scope
    'no-implicit-globals': ['error'],
    // disallow the use of eval()-like methods
    'no-implied-eval': ['error'],
    // disallow multiline strings
    'no-multi-str': ['error'],
    // disallow new operators with the Function object
    'no-new-func': ['error'],
    // disallow new operators with the String, Number, and Boolean objects
    'no-new-wrappers': ['error'],
    // disallow new operators outside of assignments or comparisons
    'no-new': ['error'],
    // disallow variable redeclaration
    'no-redeclare': ['error', {'builtinGlobals': true}],
    // disallow assignment operators in return statements
    'no-return-assign': ['error', 'always'],
    // disallow assignments where both sides are exactly the same
    'no-self-assign': ['error'],
    // disallow comparisons where both sides are exactly the same
    'no-self-compare': ['error'],
    // disallow comma operators
    'no-sequences': ['error'],
    // disallow unmodified loop conditions
    'no-unmodified-loop-condition': ['error'],
    // disallow unnecessary calls to .call() and .apply()
    'no-useless-call': ['error'],
    // disallow redundant return statements
    'no-useless-return': ['error'],

    /**
     * Variables
     */

    // disallow deleting variables
    'no-delete-var': ['error'],
    // disallow specified global variables
    'no-restricted-globals': ['error', 'event'],
    // disallow initializing variables to undefined
    'no-undef-init': ['error'],
    // disallow the use of undeclared variables unless mentioned in /*global */
    // comments
    'no-undef': ['error'],
    // disallow unused variables
    'no-unused-vars': ['warn'],
    // disallow the use of variables before they are defined
    'no-use-before-define': ['error', {'functions': false}],

    // require or disallow semicolons instead of ASI
    // 'semi': ['error', 'always']

    /**
    * ECMAScript 6
    */

    // disallow reassigning const variables
    'no-const-assign': ['error']

  }

};
